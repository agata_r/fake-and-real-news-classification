# Fake and real news classification

## Description

Classification task - predicting, whether particular text is fake news or not.

## Content

- Cleaning data
- Data visualization using wordcloud
- Logistic regression + GridSearchCV
- Classification using Keras (LSTM)
